window.onload = () => {
	
	// let template = () => {
	// 	let newDiv = document.createElement('div');
	// 	document.body.appendChild(newDiv);
	// 	newDiv.id = 'root';
	// 	newDiv.className = 'rootclass';
		
	// }

	// template();

	// let template = () => {
	// 	//const root = document.getElementById('root');
	// 	const root = document.querySelector('#root');
	// 	console.log(root);
	// 	const header = document.createElement('header');
	// 		console.log(header);
	// 	header.classList.add('header');
	// 	root.append(header);
	// 	template();
	// }

	const root = document.querySelector('#root');
// создание элементов и добавление классов
	let addElement = (elem, clas, parent) => {
		const el = document.createElement(elem);
		if (clas) {
			el.classList.add(clas);
		}
		parent.append(el);
		return el;
	}
// создание элементов и добавление атрибутов
	let addAttribute = (elem, parent, atr, value) => {
		const el = document.createElement(elem);
		parent.append(el);
		el.setAttribute(atr, value);
		return el;
	}

	const header = addElement('header', 'header', root);
	const logo = addElement('div', 'logo', header);
	const logoA = addAttribute('a', logo, 'href', '#');
	const menu = addElement('nav', 'menu', header);
	const ul = addElement('ul', '', menu);
	const liFirst = addElement('li', '', ul);
	const liFirstA = addAttribute('a', liFirst, 'href', '#');
	const liLast = addElement('li', '', ul);
	const liLastA = addAttribute('a', liLast, 'href', '#');
	liFirstA.innerHTML = 'Главная';
	liLastA.innerHTML = 'Персонажи';
	const content = addElement('div', 'content', root);
	const text = addElement('div', 'text', content);
	const textFirst = addElement('p', '', text);
	const textLast = addElement('p', '', text);
	textFirst.innerHTML = 'Найди любимого<br> персонажа<br> "Гарри Поттера"';
	textLast.innerHTML = 'Вы сможете узнать тип героев, их<br> способности, силные строны и недостатки';
	const button = addAttribute('button', content, 'href', '#');
	button.innerHTML = 'Начать';
	

}